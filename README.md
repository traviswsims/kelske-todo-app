## Setup

1. Open a terminal in the project directory, for example `~/Development/kelske-todo-app`
2. In your terminal, start VimClojure with `lein vimclojure`.  This will give you several Clojure tools, such as the REPL (read-eval-print-loop) in Vim.
3. Open another terminal window and navigate to the project directory.
4. In the new terminal window, start Figwheel with `lein figwheel`.  This creates a development web server that automatically compiles your ClojureScript code to JavaScript, and reloads your web browser.
5. Open Chrome (seriously, use Chrome for web development) to [http://localhost:3449](http://localhost:3449).  This points your browser to your computer (`localhost`) on port 3449, where your Figwheel server is running.
6. Open another terminal window and navigate to the project directory.
7. In your third terminal window, open MacVim with `vim`.

## Vim Use

* To browse your files, open NerdTree with `\e`.  Just type `?` to open (or close) the NerdTree help.  It's simple, and well-written.

### Modes

Vim has a few modes, but the big three are: **insert**, **normal**, and **visual**.  You can see what mode you're in (as well as many other things) on the colored line at the bottom of Vim (called the "status line").


#### Insert Mode

This is when you're typing (inserting) text, like you would in a standard word processor.  This is not the default mode, but there are a few ways to get to insert mode: `i`, `a`, and `o` (see below).


#### Normal Mode

Normal mode is the default mode that allows you to execute commands, move the cursor, find (and replace) text, etc.  At any time, you can get to normal mode by pressing `esc`.
As soon as you are finished typing something, *always* go back to normal mode.  It's a great habit that will make you more efficient.

Some normal mode commands:
* `:w` `enter` - write (save) the buffer ("window" you're in)
* `:q` `enter` - quit the buffer
* `i`  - insert (start typing left of) where your cursor is
* `I`  - insert at the beginning of the line
* `a`  - append (start typing right of) where your cursor is
* `A`  - append at the end of the line
* `o`  - open a new line below the line you're on, and go to insert mode
* `O`  - open a new line below the line you're on, and go to insert mode
* `yy` - yank (copy) the current line
* `p`  - paste yanked (copied) text right of the cursor
* `P`  - paste yanked (copied) text left of the cursor
* `u`  - undo
* `Control+r` - redo
* `x` - delete the character under the cursor

Notice the pattern of the above commands!  A lowercase version does something before your cursor location (cursor, line, etc.), the uppercase version does something after your location.

Here's the power of Vim:  commands and like words that can be combined into sentences (higher level commands).  For example:  `:w` is save, `:q` is quit.  You can also do this in one step: `:wq`.
Or, if you want to delete the next five characters, instead of typing `xxxxx`, just type `5x`.

Because this is true, movement (among others) is very easy and powerful!


##### Movement

Movement occurs in normal mode, and your "arrow keys" are:

* `h` - left
* `j` - down
* `k` - up
* `l` - right

Since Vim can combine commands, you can move up three lines by typing `3j`, or right seven characters by typing `7h`.

Some other movement commands:

* `gg` - go to line 1 (beginning of file)
* `<Number>gg` - go to line number.  For example, go to line 54 with `54gg`.  This is one reason why the line numbers are on the left of the file.
* `G` - go to the last line (end of file)
* `b` - go back a word.  Go back four words with `4b`.
* `w` - go forward a word.  Go forward nine words with `9w`.
* `^` - go to the beginning of the line, stay in normal mode
* `$` - go to the end of the line, stay in normal mode


#### Visual Mode

This is just "highlighting" text so you can copy, cut, or delete.  To go to visual mode, type `v` from normal mode, in a couple different ways:

* `v` - highlight the character under the cursor
* `V` - highlight the current line
* `Control+v` - visual block (vertical highlighting instead of horizontal)

Once in visual mode, use your movement commands to "highlight" the correct text.  Then you can delete the highlighted text with `x` or yank (copy) it with `y`.
Any time something is deleted in Vim, it automatically goes to your "yank buffer" (Vim-speak for clipboard).


### Putting it all together

Example:  I want to highlight the next three words, copy them, and paste them at the end of line 31.

From normal mode (`esc` if you're not already there), type: `v3wy31gg$p`.  Don't get nervous, and don't think you have to think of the whole command at once.  You could execute these keystrokes hours apart and it would still work!

Break it down:
1. `v` - go to visual mode (to highlight text)
2. `3w` - go forward three words.  Since you're in visual mode, moving will expand your highlighted area.
3. `y` - copy the text under the cursor.  Automatically ends visual mode.
4. `31gg` - go to line 31.
5. `$` - go the end of the current line
6. `p` - paste what you copied.


### Canceling a command

Just type `esc` if you make a typo during a command sequence.  For example, suppose you want to go to line 15, but you accidentally typed 35, realized it, but haven't hit `gg` yet.  Just escape:

1. `35` - oops.  You meant to hit `15`.
2. `esc` - takes you back to normal mode, with no active commands
3. `15gg` - go to line 15


## Searching

Search from normal mode with `/` followed by what you're looking for.  For example, search for "hello" with `/hello`.  You can see what you're typing at the bottom of Vim, and the results will be highlighted.  Some commands

* `n` - next result, forward
* `N` - next result, backward

You can also search for the word under the cursor in normal mode by typing `#`.

Search has some **really** powerful capabilities in Vim, but that'll get you started.  More would be overwhelming, I think.
