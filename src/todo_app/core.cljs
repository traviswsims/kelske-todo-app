(ns todo-app.core
  (:require
    [reagent.core :as reagent :refer [atom]]
    [todo-app.todo-list :refer [todo-list]]
    [todo-app.db :refer [state]]
    ))

(enable-console-print!)

(reagent/render-component [todo-list]
                          (. js/document (getElementById "app")))

(defn on-js-reload
  []
  (swap! state update-in [:__figwheel_counter] inc)
  )
