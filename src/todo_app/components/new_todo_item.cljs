(ns todo-app.components.new-todo-item
  (:require
    [reagent.core :refer [atom]]
    [reagent-forms.core :refer [bind-fields]]
    [todo-app.db :refer [state]]
    ))

(defn add-item
  [tmp]
  (let [items (:items @state)
        new-item {:text   (:new-item @tmp)
                  :status :incomplete
                  :id     (count items)}]
    (swap! state assoc :items (conj items new-item))
    (swap! tmp dissoc :new-item)))

(defn new-item-form
  []
  (let [tmp (atom {})]
    (fn []
      [:div

       [:div.form-group
        [bind-fields
         [:input.form-control {:field :text :id :new-item :placeholder "New Item"}]
         tmp]
        ]

       [:div.form-group
        [:button.btn.btn-info {:on-click (fn [_] (add-item tmp))} "Add an Item"]
        ]
       ])))
