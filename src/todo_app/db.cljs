(ns todo-app.db
  (:require
    [reagent.core :refer [atom]]
    ))

(defonce state
  (atom {:user  {:first-name "Bob"
                 :last-name "Smith"}
         :items [{:text   "Cook dinner"
                  :status :incomplete
                  :id     0}
                 {:text   "Wash the car"
                  :status :incomplete
                  :id     1}
                 ]}))
