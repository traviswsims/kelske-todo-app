(ns todo-app.todo-list
  (:require
    [todo-app.components.new-todo-item :as new-item]
    [todo-app.db :refer [state]]
    ))

(defn todo-list
  []
  [:div.container
   [:h1 "To-Do List"]

   [:ol
    (doall (for [item (:items @state)]
             [:li {:key (str "item-" (:id item))} (:text item)]))
    ]

   [new-item/new-item-form]

   ])
